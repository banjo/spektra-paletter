package ge.init5.paletter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by __ted__ on 2/10/17.
 */

public class KmeanAsync extends AsyncTask<Bitmap, Void, Map<Integer, List<Point>>> {

    Button[] colors;
    ImageView[] trackballs;

    ProgressDialog mProgress;
    Context context;


    public KmeanAsync(Context context, Button[] colors, ImageView[] trackballs){
        this.colors = colors;
        this.trackballs = trackballs;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mProgress = new ProgressDialog(context);
        mProgress.setMessage("Generating Colors...");
        mProgress.show();
    }

    @Override
    protected void onPostExecute(Map<Integer, List<Point>> map) {
        super.onPostExecute(map);



        int zz=0;
        for(Map.Entry<Integer, List<Point>> m : map.entrySet()){
            colors[zz].setBackgroundColor(m.getKey());
            colors[zz].setText(String.format("#%06X", 0xFFFFFF & m.getKey()));
            EditActivity.ChangeValueColors(colors[zz]);
            Point middle = m.getValue().get(m.getValue().size()/2);
            SetColor(trackballs[zz], middle);
            zz++;
        }



        if (mProgress.getWindow() != null) {
            mProgress.dismiss();
        }
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Map<Integer, List<Point>> doInBackground(Bitmap... params) {

        Bitmap bitmap = params[0];

        int[] cc = DominantColors.getColors(bitmap, 5);
        List<Integer> cs = new ArrayList<>();
        for (int z : cc) {
            cs.add(z);
        }

        Map<Integer, List<Point>> map = new HashMap<>();





        map = new HashMap<>();
        for (int ind = 0; ind < 5; ind++) {

            int color = cc[ind];
            map.put(color, new ArrayList<Point>());

            double minDiff = DominantColors.DEFAULT_MIN_DIFF;

            while (true) {

                List<Point> pointList = map.get(color);

                for (int i = 0; i < bitmap.getHeight(); i++) {

                    for (int j = 0; j < bitmap.getWidth(); j++) {

                        int col = bitmap.getPixel(j, i);

                        if (DominantColors.calculateDistance(col, color) < minDiff) {
                            pointList.add(new Point(j, i, col));
                        }
                    }

                }
                if(pointList.size() == 0)
                    minDiff *=2;
                else break;

            }
        }



        return map;
    }

    private void SetColor(ImageView trackBall, Point point){

        trackBall.setX(point.x-trackBall.getWidth()/2);
        trackBall.setY(point.y-trackBall.getHeight()/2);


        GradientDrawable bgShape = (GradientDrawable)trackBall.getBackground();
        bgShape.setColor(point.color);

    }
}