package ge.init5.paletter;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShareActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.share_button)
    Button shareButton;

    @BindView(R.id.download_button)
    Button downloadButton;

    @BindView(R.id.open_in_spektra_button)
    Button openInSpektraButton;

    @BindView(R.id.start_over_button)
    Button startOverButton;

    @BindView(R.id.exported_image)
    ImageView exportedImage;

    @BindView(R.id.appbar_layout)
    AppBarLayout appBarLayout;
//
//    private Uri exportedUri;
//
//    private int[] exportedColors;

    private Intent exportedIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        shareButton.setOnClickListener(this);
        shareButton.setCompoundDrawables(null, new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_share)
                .color(ContextCompat.getColor(this, R.color.cardview_light_background))
                .sizeDp(30), null, null);

        downloadButton.setOnClickListener(this);
        downloadButton.setCompoundDrawables(null, new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_save)
                .color(ContextCompat.getColor(this, R.color.cardview_light_background))
                .sizeDp(30), null, null);

        openInSpektraButton.setOnClickListener(this);
        openInSpektraButton.setCompoundDrawables(null, new IconicsDrawable(this)
                .icon(GoogleMaterial.Icon.gmd_desktop_mac)
                .color(ContextCompat.getColor(this, R.color.cardview_light_background))
                .sizeDp(30), null, null);

        startOverButton.setOnClickListener(this);

        Intent i = getIntent();
        Uri uri = i.getData();
        int[] colors = i.getIntArrayExtra("colors");

        if (uri != null && colors != null) {

//            exportedUri = uri;
//            exportedColors = colors;
            exportedIntent = i;

            exportedImage.setImageURI(uri);

            exportedImage.post(new Runnable() {
                @Override
                public void run() {
                    CoordinatorLayout.LayoutParams paramss = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
                    paramss.height = appBarLayout.getWidth();

                    appBarLayout.setLayoutParams(paramss);
                }
            });

        }


    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        MenuItem main_item2 = menu.add(0, 0, 0, "Start Over");
        main_item2.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public Intent SharingToSocialMedia() {


        final Intent share = new Intent(Intent.ACTION_SEND);
        share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        share.putExtra(Intent.EXTRA_STREAM, exportedIntent.getData());
        share.setType("image/png");

//        startActivity(Intent.createChooser(share, "Title of the dialog the system will open"));
        return share;
    }

    public void SharingPalette(){
        final Intent share = new Intent(Intent.ACTION_SEND);
        share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        share.putExtras(exportedIntent);
        share.setPackage("ge.init5.spektra");
        share.setType("palette/*");
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(share, 0);
        if(resInfo.isEmpty()){
            Toast.makeText(this, "Please Install Spektra", Toast.LENGTH_SHORT).show();
        }else {
            startActivity(share);
//            finish();
        }
    }


    public void ShareExcludingApp() {

        List<Intent> targetedShareIntents = new ArrayList<Intent>();
//        Intent share = new Intent(android.content.Intent.ACTION_SEND);
//        share.setType("image/*");
        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(SharingToSocialMedia(), 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                Intent targetedShare = SharingToSocialMedia();

                if (!info.activityInfo.packageName.equalsIgnoreCase("ge.init5.paletter")) {
                    targetedShare.setPackage(info.activityInfo.packageName);
                    targetedShareIntents.add(targetedShare);
                }
            }

            Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0),
                    "Select app to share");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                    targetedShareIntents.toArray(new Parcelable[]{}));
            startActivity(chooserIntent);
        }

    }





    public File bitmapConvertToFile() {

        File file = new File(Environment.getExternalStoragePublicDirectory("Paletter"), "");

        if (!file.exists()) {
            file.mkdir();
        }

        File bitmapFile = new File(exportedIntent.getData().getPath());

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable=true;
            Bitmap imageBitmap = BitmapFactory.decodeFile(bitmapFile.getPath(), options);
//        Bitmap roundCorners = Bitmap.createScaledBitmap(((BitmapDrawable) getResources().getDrawable(R.drawable.round)).getBitmap(),
//                EditActivity.CARD_DIM, EditActivity.CARD_DIM, true);
//
//        Canvas canvas = new Canvas(imageBitmap);
//
//        Paint eraser = new Paint();
//        eraser.setAntiAlias(true);
//        eraser.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
//        canvas.drawBitmap(roundCorners, 0, 0, eraser);



        File downloadedFIle = new File(file, "IMG_" + (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime()) + ".jpg");
        try {

            FileOutputStream outputStream = new FileOutputStream(downloadedFIle);
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

            Toast.makeText(this, "Image Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MediaScannerConnection.scanFile(this, new String[]{downloadedFIle.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
            @Override
            public void onMediaScannerConnected() {
            }

            @Override
            public void onScanCompleted(final String path, Uri uri) {
            }
        });

        return bitmapFile;
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.share_button:
                ShareExcludingApp();
                break;
            case R.id.download_button:
                bitmapConvertToFile();
                break;
            case R.id.open_in_spektra_button:
                SharingPalette();
//                Toast.makeText(this, "Calm your pussy down", Toast.LENGTH_LONG).show();
                break;
            case R.id.start_over_button:
                new File(exportedIntent.getData().getPath()).delete();
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                break;
        }
    }
}
