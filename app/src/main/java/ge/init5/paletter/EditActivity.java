package ge.init5.paletter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.media.Image;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnTouch;
import it.sephiroth.android.library.easing.Linear;

public class EditActivity extends AppCompatActivity {

    public static boolean SHOW_VALUES = false;

    public static int CARD_DIM = 1000;


    @BindView(R.id.editor)
    FrameLayout editorLayout;

    @BindView(R.id.editor_flipper)
    ViewFlipper flipper;

    @BindView(R.id.mask)
    ImageView mask;

    @BindView(R.id.color_generator_button)
    Button generatorButton;

    @BindView(R.id.values_switcher)
    SwitchCompat valueSwitcher;

    @BindView(R.id.watermark_switcher)
    SwitchCompat watermarkSwitcher;

    FrameLayout frame;


    ImageView mainImage;

    Button[] colors;
    ImageView[] trackballs;



    private View currentTrackBall;

    private Button currentColorView;

    private Bitmap scaledBitmap;



    private float dX = 0;
    private float dY = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        ButterKnife.bind(this);
        SHOW_VALUES = false;

        if(MainActivity.RATIO_WIDTH > MainActivity.RATIO_HEIGHT) {
            Log.d("EDITACTIVITY", "Landscape");
            flipper.setDisplayedChild(1);
        }



        valueSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SHOW_VALUES = valueSwitcher.isChecked();
                for (Button b : colors)
                    ChangeValueColors(b);
            }
        });

        watermarkSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Snackbar.make(buttonView, "You Cant Right Now",Snackbar.LENGTH_LONG).show();
                watermarkSwitcher.setChecked(true);
            }
        });


        View currentView = flipper.getCurrentView();

        frame = (FrameLayout) currentView.findViewById(R.id.frame_layout);
        mainImage = (ImageView) currentView.findViewById(R.id.main_picture);

        colors = new Button[5];
        trackballs = new ImageView[5];

        for (int i=0; i<5; i++) {
            colors[i] = ((Button) currentView.findViewById(getResources().getIdentifier("color_" + (i + 1), "id", getPackageName())));
            trackballs[i] = ((ImageView) currentView.findViewById(getResources().getIdentifier("trackball" + (i + 1), "id", getPackageName())));
        }

//        editorLayout.setLayoutParams(new RelativeLayout.LayoutParams(editorLayout.getWidth(), editorLayout.getWidth()));

        currentTrackBall = trackballs[0];
        currentColorView = colors[0];

        Intent i = getIntent();
        Uri uri = i.getData();


        if(uri != null) {


            mainImage.setImageURI(uri);


            new File(uri.getPath()).delete();

            mainImage.post(new Runnable() {
                @Override
                public void run() {

                    RelativeLayout.LayoutParams paramss = (RelativeLayout.LayoutParams) editorLayout.getLayoutParams();
                    paramss.height = editorLayout.getWidth();

                    editorLayout.setLayoutParams(paramss);

//                    Log.d("EDITORLAYOUT", mainImage.getWidth() + "");
//                    Log.d("EDITORLAYOUT", mainImage.getHeight() + "");


                    scaledBitmap = Bitmap.createScaledBitmap(((BitmapDrawable) mainImage.getDrawable()).getBitmap(),
                            mainImage.getWidth(), mainImage.getWidth() * MainActivity.RATIO_HEIGHT / MainActivity.RATIO_WIDTH, true);


                    mainImage.setImageBitmap(scaledBitmap);

                    ViewGroup.LayoutParams params = mask.getLayoutParams();
                    params.height = paramss.height;

                    mask.setLayoutParams(params);

                    GenerateColors();
                }
            });
        }


        for (Button color : colors){
            color.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    currentColorView = (Button) v;
                    int vIndex = Integer.parseInt(v.getTag().toString());

                    for (int i = 0; i < trackballs.length; i++) {
                        if(i == vIndex) {
                            currentTrackBall = trackballs[i];
                            trackballs[i].setVisibility(View.VISIBLE);
                        }
                        else
                            trackballs[i].setVisibility(View.INVISIBLE);
                    }
                }
            });
        }

        frame.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    

                    float maxWidth = frame.getWidth() - currentTrackBall.getWidth()/2 - 1;
                    float maxHeight = frame.getHeight() - currentTrackBall.getHeight()/2 - 1;

                    float minWidth = -currentTrackBall.getWidth()/2 + 1;
                    float minHeight = -currentTrackBall.getHeight()/2 + 1;


                    float x = event.getX();
                    float y = event.getY();

                    switch(event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN :
                            dX = currentTrackBall.getX() - x;
                            dY = currentTrackBall.getY() - y;

                            return true;
                        case MotionEvent.ACTION_MOVE :

                            float mX = x+dX;
                            float mY = y+dY;
//                            Log.d("TOUCH Action " , "Move, Delta: (" + mX + ", " + mY+")");


                            mX = CheckBounds(mX, minWidth, maxWidth);
                            mY = CheckBounds(mY, minHeight, maxHeight);

                            currentTrackBall.setX(mX);
                            currentTrackBall.setY(mY);

                            GetColor(currentTrackBall);

                            return true;
                        case MotionEvent.ACTION_UP :
                            ChangeValueColors(currentColorView);
                            return true;
                    }
                    return false;
                }
            }

        );

    }







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.main_menu, menu);
        MenuItem edit_item = menu.add(0, 0, 0, "Next");
        edit_item.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case 0:

                Intent newIntent = new Intent(EditActivity.this, ShareActivity.class);
                Uri uri = GenerateImage();
                if(uri != null) {
                    newIntent.setData(uri);
                    int[] cc = new int[5];
                    for (int i = 0; i < colors.length; i++) {
                        cc[i] = ((ColorDrawable)colors[i].getBackground()).getColor();
                    }
                    newIntent.putExtra("colors", cc);
                    startActivity(newIntent);
                }

                break;
            case android.R.id.home:
                onBackPressed();
            default:
                break;
        }

        return true;
    }


    public void GenerateColors(){
        Bitmap bitmap = ((BitmapDrawable)mainImage.getDrawable()).getBitmap();
        new KmeanAsync(EditActivity.this, colors, trackballs).execute(bitmap);

    }


    public Uri GenerateImage(){

        boolean IsPortrait = MainActivity.RATIO_WIDTH < MainActivity.RATIO_HEIGHT;


        int imageWidth = CARD_DIM * MainActivity.RATIO_WIDTH/10;
        int imageHeight = CARD_DIM * MainActivity.RATIO_HEIGHT/10;
        float padding = CARD_DIM*0.015f;

        float lastX = 0;
        float lastY = 0;

        float colorWidth = CARD_DIM * (IsPortrait ? 0.285f : 0.188f);
        float colorHeight= CARD_DIM * (IsPortrait ? 0.188f : 0.285f);

        float watermarkWidth = Math.max(colorWidth, colorHeight);
        float watermarkHeight = Math.min(colorWidth, colorHeight);

        Bitmap watermark = Bitmap.createScaledBitmap(((BitmapDrawable) getResources().getDrawable(R.drawable.apptitlelogo)).getBitmap(),
                (int)watermarkWidth, (int)watermarkHeight, true);


        Bitmap bitmap = Bitmap.createScaledBitmap(scaledBitmap, imageWidth, imageHeight, true);

        Bitmap imageBitmap = Bitmap.createBitmap(CARD_DIM, CARD_DIM, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(imageBitmap);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        Paint white = new Paint();
        white.setColor(Color.WHITE);

        // draw main picture
        canvas.drawBitmap(bitmap, 0, 0, paint);


        if(IsPortrait)
            lastX = imageWidth+padding;
        else
            lastY = imageHeight+padding;

        // draw vertical/horizontal padding line
        if(IsPortrait)
            canvas.drawRect(imageWidth, 0, lastX, imageHeight, white);
        else
            canvas.drawRect(0, imageHeight, imageWidth, lastY, white);


        Paint colorBrush = new Paint();
        Paint eyeColor = new Paint();
        eyeColor.setAntiAlias(true);
        Paint watermarkColor = new Paint();

        eyeColor.setTextSize(26);
        // draw color rects
        for (int i = 0; i < 5; i++){
            int colorValue = ((ColorDrawable)colors[i].getBackground()).getColor();
            colorBrush.setColor(colorValue);
            eyeColor.setColor(GetRightColor(colorValue));
            if(i!=0) {
                if(IsPortrait) {
                    canvas.drawRect(lastX, lastY, lastX + colorWidth, lastY + padding, white);
                    lastY += padding;
                }else{
                    canvas.drawRect(lastX, lastY, lastX + padding, lastY + colorHeight, white);
                    lastX += padding;
                }
            }else{
                watermarkColor.setColor(eyeColor.getColor());
            }

            canvas.drawRect(lastX, lastY, lastX + colorWidth, lastY+colorHeight, colorBrush);

            if(SHOW_VALUES){
                if(IsPortrait)
                    canvas.drawText(String.format("#%06X", 0xFFFFFF & colorValue), lastX + colorWidth/10, lastY + colorHeight - colorHeight/5, eyeColor);
                else
                    canvas.drawText(String.format("#%06X", 0xFFFFFF & colorValue), lastX + colorWidth/5, lastY + colorHeight - colorHeight/10, eyeColor);
            }

            if(IsPortrait)
                lastY += colorHeight;
            else
                lastX += colorWidth;


        }

        if(IsPortrait)
            canvas.drawBitmap(watermark, CARD_DIM - watermarkWidth, 0, watermarkColor);
        else
            canvas.drawBitmap(watermark, CARD_DIM - watermarkWidth, imageHeight - watermarkHeight, watermarkColor);
        try {

            File file = File.createTempFile("crop", ".png", this.getExternalCacheDir());
            FileOutputStream outputStream = new FileOutputStream(file);
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);


            return Uri.fromFile(file);


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    private float CheckBounds(float c, float lBound, float hBound){

        if (c < lBound){
            c = lBound;
        }
        else if(c > hBound){
            c = hBound;
        }

        return c;
    }


    private void GetColor(View trackBall){
        int cX = (int)trackBall.getX()+trackBall.getWidth()/2;
        int cY = (int)trackBall.getY()+trackBall.getHeight()/2;


        Bitmap bitmap = ((BitmapDrawable)mainImage.getDrawable()).getBitmap();

        int pixel = bitmap.getPixel(cX,cY);

        GradientDrawable bgShape = (GradientDrawable)trackBall.getBackground();
        bgShape.setColor(pixel);

        currentColorView.setBackgroundColor(pixel);
        currentColorView.setText(String.format("#%06X", 0xFFFFFF & pixel));
        currentColorView.setTextColor(pixel);



//        trackBall.setBackgroundColor(pixel);
    }


    public static void ChangeValueColors(Button button){

        int c = ((ColorDrawable)button.getBackground()).getColor();

        if(!SHOW_VALUES)
            button.setTextColor(c);
        else{

            button.setTextColor(GetRightColor(c));
        }
    }

    private static int GetRightColor(int c){
        int d = 0;

        double a = 1 - ( 0.299 * Color.red(c) + 0.587 * Color.green(c) + 0.114 * Color.blue(c))/255;

        if (a < 0.5)
            d = 0; // bright colors - black font
        else
            d = 255; // dark colors - white font

        return Color.argb(200, d, d, d);
    }


    public void GenerateButton(View v){
        GenerateColors();
    }


}
