package ge.init5.paletter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.naver.android.helloyako.imagecrop.view.ImageCropView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import ge.init5.paletter.MediaAdapter.GridAdapter;
import ge.init5.paletter.MediaAdapter.GridAdapterListener;

public class MainActivity extends AppCompatActivity implements GridAdapterListener {

    public static final String TAG = "MainActivity";

    @BindView(R.id.mGalleryRecyclerView)
    RecyclerView mGalleryRecyclerView;

    @BindView(R.id.mPreview)
    ImageCropView mPreview;



    private static final String EXTENSION_JPG = ".jpg";
    private static final String EXTENSION_JPEG = ".jpeg";
    private static final String EXTENSION_PNG = ".png";
    private static final int MARGING_GRID = 2;

    public static int RATIO_WIDTH = 10;
    public static int RATIO_HEIGHT = 7;

    private GridAdapter mGridAdapter;
    private ArrayList<File> mFiles;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);



        mGridAdapter = new GridAdapter(getApplicationContext());

        mGridAdapter.setListener(this);
        mGalleryRecyclerView.setAdapter(mGridAdapter);
        mGalleryRecyclerView.setHasFixedSize(true);
		GridLayoutManager mGridLayoutManager = new GridLayoutManager(getApplicationContext(), 4);
		//mGridLayoutManager.setReverseLayout(true);
        mGalleryRecyclerView.setLayoutManager(mGridLayoutManager);
        mGalleryRecyclerView.addItemDecoration(addItemDecoration());

//        CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) mAppBarContainer.getLayoutParams();
//        lp.height = getResources().getDisplayMetrics().widthPixels;
//        mAppBarContainer.setLayoutParams(lp);

        fetchMedia();

        ChangeAspectRatio();



    }

    private RecyclerView.ItemDecoration addItemDecoration() {
        return new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view,
                                       RecyclerView parent, RecyclerView.State state) {
                outRect.left = MARGING_GRID;
                outRect.right = MARGING_GRID;
                outRect.bottom = MARGING_GRID;
                if (parent.getChildLayoutPosition(view) >= 0 && parent.getChildLayoutPosition(view) <= 4) {
                    outRect.top = MARGING_GRID;
                }
            }
        };
    }

    private void fetchMedia() {
        mFiles = new ArrayList<>();
        File dirPubDownloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        parseDir(dirPubDownloads);
        File dirPubDcim = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        parseDir(dirPubDcim);
        File dirPubPictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        parseDir(dirPubPictures);

//		File dirDownloads = Environment.getExternalStorageDirectory();
//        parseDir(dirDownloads);



        Collections.sort(mFiles, new Comparator<File>() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public int compare(File o1, File o2) {
                    return Long.compare(o2.lastModified(), o1.lastModified());
                }
            }
        );
        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        String imagePath = null;

        if (mFiles.size() > 0) {
            mGridAdapter.setItems(mFiles);
            imagePath = Uri.fromFile(mFiles.get(0)).getPath();
        } else {
//            mMediaNotFoundWording.setVisibility(View.VISIBLE);
        }

        if (Intent.ACTION_SEND.equals(action) && type != null && type.startsWith("image/")) {
            Uri uri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (uri != null) {
                imagePath = getRealPathFromURI(uri);
            }
        }

        if(imagePath != null)
            mPreview.setImageFilePath(imagePath);


    }


    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void parseDir(File dir) {
        File[] files = dir.listFiles();
        if (files != null) {
            parseFileList(files);
        }
    }

    private void parseFileList(File[] files) {
        for (File file : files) {
            if (file.isDirectory()) {
                if (!file.getName().toLowerCase().startsWith(".")) {
                    parseDir(file);
                }
            } else {
                if (file.getName().toLowerCase().endsWith(EXTENSION_JPG)
                        || file.getName().toLowerCase().endsWith(EXTENSION_JPEG)
                        || file.getName().toLowerCase().endsWith(EXTENSION_PNG)) {
                    mFiles.add(0, file);
                }
            }
        }
    }

    private void displayPreview(File file) {

        mPreview.setImageFilePath(Uri.fromFile(file).getPath());

    }

    private void ChangeAspectRatio(){
        int temp = RATIO_HEIGHT;
        RATIO_HEIGHT = RATIO_WIDTH;
        RATIO_WIDTH = temp;

        mPreview.setAspectRatio(RATIO_WIDTH, RATIO_HEIGHT);
    }




    @Override
    public void onClickMediaItem(File file) {
        displayPreview(file);
//        mAppBarContainer.setExpanded(true, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem main_item1 = menu.add(0, 1, 0, "Change Layout");
        main_item1.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        MenuItem main_item2 = menu.add(0, 0, 0, "Next");
        main_item2.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case 0:
                if (!mPreview.isChangingScale()) {
                    Bitmap b = mPreview.getCroppedImage();
                    if (b != null) {
                        new CropAsync(MainActivity.this).execute(b);
                    } else {
                        Toast.makeText(MainActivity.this, "FAILED BITCH", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case 1:
                ChangeAspectRatio();
            default:
                break;
        }

        return true;
    }



}
