package ge.init5.paletter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.naver.android.helloyako.imagecrop.view.ImageCropView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static ge.init5.paletter.R.id.mPreview;

/**
 * Created by __ted__ on 2/10/17.
 */

public class CropAsync extends AsyncTask<Bitmap, Void, Void>{

    Context context;

    ProgressDialog mProgress;

    public CropAsync(Context context){
        this.context = context;
    }


    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Void doInBackground(Bitmap... params) {
        Bitmap bitmap = params[0];
        try{
            File file = File.createTempFile("crop", ".png", context.getExternalCacheDir());
            FileOutputStream outputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            Uri path = Uri.fromFile(file);

            Intent newIntent = new Intent(context, EditActivity.class);
            newIntent.setData(path);
            context.startActivity(newIntent);
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        mProgress = new ProgressDialog(context);
        mProgress.setMessage("Loading image...");
        mProgress.show();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (mProgress.getWindow() != null) {
            mProgress.dismiss();
        }
    }


}
