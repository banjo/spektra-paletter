package ge.init5.paletter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import java.util.zip.Inflater;

/**
 * Created by __ted__ on 2/10/17.
 */

public class EditorLayout extends FrameLayout {


    public EditorLayout(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewPortrait = inflater.inflate(R.layout.editor_portrait, null);
        View viewLandscape = inflater.inflate(R.layout.editor_landscape, null);

        ViewFlipper flipper = new ViewFlipper(context);
        flipper.addView(viewPortrait);
        flipper.addView(viewLandscape);

        addView(flipper);

        ImageView mask = new ImageView(context);
        mask.setBackground(getResources().getDrawable(R.drawable.rounded_mask));

        addView(mask);
    }

    public EditorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public EditorLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);


//        Bitmap bitmap = Bitmap.createScaledBitmap(((BitmapDrawable)mainImage.getDrawable()).getBitmap(),
//                frame.getWidth(), frame.getHeight(), true);
//
//        mainImage.setImageBitmap(bitmap);
//
//        ViewGroup.LayoutParams params = mask.getLayoutParams();
//        params.height = mainImage.getHeight();
//
//        mask.setLayoutParams(params);
//
//        GenerateColors();
    }
}
